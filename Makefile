OUT_DIR = bin
REPORT = main
RESUME = resume
EN_DIR = en
PT_DIR = pt

.PHONY: build clean

build: $(OUT_DIR)/$(EN_DIR)-$(RESUME).pdf $(OUT_DIR)/$(PT_DIR)-$(RESUME).pdf

clean:
	@rm -rf $(OUT_DIR)/*

write-en: $(EN_DIR)/$(REPORT).tex
	latexmk -pdf -pvc -cd $< -aux-directory=$(OUT_DIR) -output-directory=$(OUT_DIR)

write-pt: $(PT_DIR)/$(REPORT).tex
	latexmk -pdf -pvc -cd $< -aux-directory=$(OUT_DIR) -output-directory=$(OUT_DIR)

$(OUT_DIR)/$(EN_DIR)-$(RESUME).pdf: $(EN_DIR)/$(REPORT).tex
	latexmk -pdf -cd $< -aux-directory=$(OUT_DIR) -output-directory=$(OUT_DIR) -jobname=$(EN_DIR)-$(RESUME)

$(OUT_DIR)/$(PT_DIR)-$(RESUME).pdf: $(PT_DIR)/$(REPORT).tex
	latexmk -pdf -cd $< -aux-directory=$(OUT_DIR) -output-directory=$(OUT_DIR) -jobname=$(PT_DIR)-$(RESUME)
